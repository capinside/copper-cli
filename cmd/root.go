package cmd

import (
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/capinside/copper-cli/cmd/activities"
	"gitlab.com/capinside/copper-cli/cmd/companies"
	"gitlab.com/capinside/copper-cli/cmd/customersources"
	"gitlab.com/capinside/copper-cli/cmd/customfields"
	"gitlab.com/capinside/copper-cli/cmd/leads"
	"gitlab.com/capinside/copper-cli/cmd/opportunities"
	"gitlab.com/capinside/copper-cli/cmd/persons"
	"gitlab.com/capinside/copper-cli/cmd/pipelines"
	"gitlab.com/capinside/copper-cli/cmd/users"
	"gitlab.com/capinside/copper-cli/pkg/copper"
)

var (
	rootCmd = &cobra.Command{
		Use:               "copper-cli",
		Short:             "Copper command line utitliy",
		PersistentPreRunE: persistentPreRunEFunc,
	}

	timeFormat        = time.RFC822
	logLevel   string = "info"
	url        string
	account    string
	token      string
)

func init() {
	rootCmd.PersistentFlags().Bool("dry-run", false, "Run in dry-run mode")
	rootCmd.PersistentFlags().StringP("output", "o", "json", "Output format. Must be one of: json | wide")
	rootCmd.PersistentFlags().StringVarP(&account, "account", "", "", "Account used to connect to Copper")
	rootCmd.PersistentFlags().StringVarP(&logLevel, "log-level", "", "info", "Log level. Must be one of: panic,fatal,error,warn,info,debug,trace")
	rootCmd.PersistentFlags().StringVarP(&token, "token", "", "", "Token used to connect to Copper")
	rootCmd.PersistentFlags().StringVarP(&url, "url", "", copper.BaseURLV1, "Copper REST API URL")

	viper.SetEnvPrefix("COPPER_API")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.BindPFlag("dry-run", rootCmd.PersistentFlags().Lookup("dry-run"))
	viper.BindPFlag("output", rootCmd.PersistentFlags().Lookup("output"))
	viper.BindPFlag("log-level", rootCmd.PersistentFlags().Lookup("log-level"))
	viper.BindPFlag("url", rootCmd.PersistentFlags().Lookup("url"))
	viper.BindPFlag("account", rootCmd.PersistentFlags().Lookup("account"))
	viper.BindPFlag("token", rootCmd.PersistentFlags().Lookup("token"))

	rootCmd.AddCommand(
		activities.Command(),
		companies.Command(),
		customersources.Command(),
		customfields.Command(),
		leads.Command(),
		opportunities.Command(),
		persons.Command(),
		pipelines.Command(),
		users.Command(),
	)
}

// Command returns the root command
func Command() *cobra.Command {
	return rootCmd
}

// Execute command and return nil or an error
func Execute() error {
	return rootCmd.Execute()
}

func persistentPreRunEFunc(cmd *cobra.Command, args []string) error {
	return copper.Init(viper.GetString("url"), viper.GetString("account"), viper.GetString("token"), viper.GetString("log-level"))
}
