package customfields

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	fetchByIDCmd = &cobra.Command{
		Use:   "fetch-by-id",
		Short: "Fetch custom field defintion by ID",
		RunE:  fetchByIDRunEFunc,
	}
)

func fetchByIDRunEFunc(cmd *cobra.Command, args []string) error {
	IDs, err := utils.StringSliceToInt64Slice(args)
	if err != nil {
		return err
	}

	customFieldDefinitions, err := fetchByIDs(IDs)
	if err != nil {
		return err
	}

	return output(customFieldDefinitions)
}

func fetchByIDs(IDs []int64) ([]*copperv1.CustomFieldDefinition, error) {
	result := make([]*copperv1.CustomFieldDefinition, len(IDs))
	for i, id := range IDs {
		customFieldDefinition, err := api.FetchCustomFieldDefinitionByID(id)
		if err != nil {
			return nil, err
		}
		result[i] = customFieldDefinition
	}

	return result, nil
}
