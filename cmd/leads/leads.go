package leads

import (
	"encoding/json"
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	maximumCreatedDate      int64
	maximumInteractionCount int64
	maximumInteractionDate  int64
	minimumCreatedDate      int64
	minimumInteractionCount int64
	minimumInteractionDate  int64

	leadsCmd = &cobra.Command{
		Use:   "leads",
		Short: "Lead commands",
	}
)

func init() {
	leadsCmd.AddCommand(
		activitiesCmd,
		deleteCmd,
		searchCmd,
		fetchByID,
	)
}

// Command returns the pointer to the Command
func Command() *cobra.Command {
	return leadsCmd
}

func output(leads []*copperv1.Lead) error {
	switch viper.GetString("output") {
	case "wide":
		return outputWide(leads)
	default:
		return outputJSON(leads)
	}
}

func outputWide(leads []*copperv1.Lead) error {
	w := tabwriter.NewWriter(os.Stdout, 10, 8, 0, '\t', 0)
	defer w.Flush()

	fmt.Fprintf(w, "%s\t%s\t%s\n", "ID", "Firstname", "Lastname")
	for _, lead := range leads {
		fmt.Fprintf(w, "%d\t%s\t%s\n", lead.Id, lead.FirstName, lead.LastName)
	}

	return nil
}

func outputJSON(leads []*copperv1.Lead) error {
	return json.NewEncoder(os.Stdout).Encode(leads)
}
