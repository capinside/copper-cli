package opportunities

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	maximumCreatedDate      int64
	maximumInteractionCount int64
	maximumInteractionDate  int64
	minimumCreatedDate      int64
	minimumInteractionCount int64
	minimumInteractionDate  int64

	searchParams = copperv1.OpportunitySearchParams{}

	searchCmd = &cobra.Command{
		Use:   "search",
		Short: "Search for opoortunities",
		RunE:  searchRunEFunc,
	}
)

func init() {
	searchCmd.Flags().Int64VarP(&maximumCreatedDate, "maximum-created-date", "", -1, "The Unix timestamp of the latest date a resource was created.")
	searchCmd.Flags().Int64VarP(&maximumInteractionDate, "maximum-interaction-date", "", -1, "The Unix timestamp of the latest date of the last interaction.")
	searchCmd.Flags().Int64VarP(&minimumCreatedDate, "minimum-created-date", "", -1, "The Unix timestamp of the earliest date the resource was created.")
	searchCmd.Flags().Int64VarP(&minimumInteractionDate, "minimum-interaction-date", "", -1, "The Unix timestamp of the earliest date of the last interaction.")
	searchCmd.Flags().Int64VarP(&maximumInteractionCount, "maximum-interaction-count", "", -1, "The maximum number of interactions the resource must have had.")
	searchCmd.Flags().Int64VarP(&minimumInteractionCount, "minimum-interaction-count", "", -1, "The minimum number of interactions the resource must have had.")

}

func searchRunEFunc(cmd *cobra.Command, args []string) error {
	if minimumInteractionCount > -1 {
		searchParams.MinimumInteractionCount = &minimumInteractionCount
	}

	if maximumInteractionCount > -1 {
		searchParams.MaximumInteractionCount = &maximumInteractionCount
	}

	if minimumInteractionDate > -1 {
		searchParams.MinimumInteractionDate = &minimumInteractionDate
	}

	if maximumInteractionDate > -1 {
		searchParams.MaximumInteractionDate = &maximumInteractionDate
	}

	if minimumCreatedDate > -1 {
		searchParams.MinimumCreatedDate = &minimumCreatedDate
	}

	if maximumCreatedDate > -1 {
		searchParams.MaximumCreatedDate = &maximumCreatedDate
	}

	result, err := api.SearchAllOpportunities(searchParams)
	if err != nil {
		return err
	}

	return output(result)
}
