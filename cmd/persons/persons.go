package persons

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	outputWriter io.Writer = os.Stdout

	personsCmd = &cobra.Command{
		Use:   "persons",
		Short: "Person commands",
	}
)

func init() {
	personsCmd.AddCommand(
		activitiesCmd,
		deleteCmd,
		fetchByEMailCmd,
		fetchByID,
		searchCmd,
	)
}

// Command returns a pointer to the Cobra Command
func Command() *cobra.Command {
	return personsCmd
}

func output(persons []*copperv1.Person, w io.Writer) error {
	switch viper.GetString("output") {
	case "wide":
		return outputWide(persons, w)
	default:
		return outputJSON(persons, w)
	}
}

func outputWide(persons []*copperv1.Person, w io.Writer) error {
	tw := tabwriter.NewWriter(w, 10, 8, 0, '\t', 0)
	defer tw.Flush()

	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\n", "ID", "Firstname", "Lastname", "Company")
	for _, person := range persons {
		fmt.Fprintf(tw, "%d\t%s\t%s\t%s\n", person.Id, person.FirstName, person.LastName, person.CompanyName)
	}

	return nil
}

func outputJSON(persons []*copperv1.Person, w io.Writer) error {
	return json.NewEncoder(w).Encode(persons)
}

func outputDeleteResponse(result []*copperv1.DeleteResponse, w io.Writer) error {
	switch viper.GetString("output") {
	case "wide":
		return outputDeleteResponseWide(result, w)
	default:
		return outputDeleteResponseJSON(result, w)
	}
}

func outputDeleteResponseWide(result []*copperv1.DeleteResponse, w io.Writer) error {
	tw := tabwriter.NewWriter(w, 10, 8, 0, '\t', 0)
	defer tw.Flush()

	fmt.Fprintf(tw, "%s\t%s\n", "ID", "Deleted")
	for _, resp := range result {
		fmt.Fprintf(tw, "%d\t%v\n", resp.Id, resp.IsDeleted)
	}

	return nil
}

func outputDeleteResponseJSON(result []*copperv1.DeleteResponse, w io.Writer) error {
	return json.NewEncoder(w).Encode(result)
}
