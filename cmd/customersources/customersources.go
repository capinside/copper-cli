package customersources

import (
	"encoding/json"
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	customerSourcesCmd = &cobra.Command{
		Use:   "customer-sources",
		Short: "Customer sources commands",
	}
)

func init() {
	customerSourcesCmd.AddCommand(listCmd)
}

func Command() *cobra.Command {
	return customerSourcesCmd
}

func output(customerSources []*copperv1.CustomerSource) error {
	switch viper.GetString("output") {
	case "wide":
		return outputWide(customerSources)
	default:
		return outputJSON(customerSources)
	}
}

func outputWide(customerSources []*copperv1.CustomerSource) error {
	w := tabwriter.NewWriter(os.Stdout, 10, 8, 0, '\t', 0)
	defer w.Flush()

	fmt.Fprintf(w, "%s\t%s\n", "ID", "Name")

	for _, customerSource := range customerSources {
		fmt.Fprintf(w, "%d\t%s\n", customerSource.Id, customerSource.Name)
	}

	return nil
}

func outputJSON(customerSources []*copperv1.CustomerSource) error {
	return json.NewEncoder(os.Stdout).Encode(customerSources)
}
