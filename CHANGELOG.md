## [1.0.18](https://gitlab.com/capinside/copper-cli/compare/1.0.17...1.0.18) (2022-12-18)


### Features

* Protobuf error message and bug fixing in error handling ([2f61057](https://gitlab.com/capinside/copper-cli/commit/2f61057df22df4862b585d93b9a23bab5e7d346a))

## [1.0.17](https://gitlab.com/capinside/copper-cli/compare/1.0.16...1.0.17) (2022-11-24)


### Bug Fixes

* Environment variables not used by default ([49a802e](https://gitlab.com/capinside/copper-cli/commit/49a802e2933a976353a6afa0478ca2fc02ed71b3))

## [1.0.16](https://gitlab.com/capinside/copper-cli/compare/1.0.15...1.0.16) (2022-11-24)


### Bug Fixes

* account,token and log-level not passed to client on init ([322e7b8](https://gitlab.com/capinside/copper-cli/commit/322e7b8e3c18923c1714abac9d436c28a0efcce9))

## [1.0.15](https://gitlab.com/capinside/copper-cli/compare/1.0.14...1.0.15) (2022-11-24)


### Bug Fixes

* account and token can't be passed to CLI ([2c1c12c](https://gitlab.com/capinside/copper-cli/commit/2c1c12c44e5d37e939864301ec9e66f082e475c4))

## [1.0.14](https://gitlab.com/capinside/copper-cli/compare/1.0.13...1.0.14) (2022-11-21)


### Bug Fixes

* log level can't be set ([6284407](https://gitlab.com/capinside/copper-cli/commit/6284407b77286a4ff74eaa405cf1c3e744b2fe07))

## [1.0.13](https://gitlab.com/capinside/copper-cli/compare/1.0.12...1.0.13) (2022-11-21)


### Bug Fixes

* Slice of should be slice of pointer to match Protobuf definitions ([7a19e69](https://gitlab.com/capinside/copper-cli/commit/7a19e69ddb249322574370ec99e097a7245fb0cf))

## [1.0.12](https://gitlab.com/capinside/copper-cli/compare/1.0.11...1.0.12) (2022-11-15)


### Features

* Protobuf usage ([4818949](https://gitlab.com/capinside/copper-cli/commit/4818949c87eb9590cc3a2bdd678273ba78126151))

## [1.0.11](https://gitlab.com/capinside/copper-cli/compare/1.0.10...1.0.11) (2022-08-25)


### Features

* Add command line utility ([e17a5df](https://gitlab.com/capinside/copper-cli/commit/e17a5dfe5e32c04b1994fc9acc37c1ec03433626))

## [1.0.10](https://gitlab.com/capinside/copper-cli/compare/1.0.9...1.0.10) (2022-08-10)


### Bug Fixes

* OpportunitySearchParams does not include SearchMetadata ([65ccc23](https://gitlab.com/capinside/copper-cli/commit/65ccc23c3e7cc1f8966e3d2c03ff00273cd8ec8e))

## [1.0.9](https://gitlab.com/capinside/copper-cli/compare/1.0.8...1.0.9) (2022-08-09)


### Features

* SearchMetadata ([f1c0e86](https://gitlab.com/capinside/copper-cli/commit/f1c0e8613d8502d8ee01a39e5c9130d80f773137))

## [1.0.8](https://gitlab.com/capinside/copper-cli/compare/1.0.7...1.0.8) (2022-08-04)


### Bug Fixes

* PersonSearchParams data attributes are *int instead of *int64 ([7e2e5da](https://gitlab.com/capinside/copper-cli/commit/7e2e5daa957343b70f658c4953702e1b52abfcb7))

## [1.0.7](https://gitlab.com/capinside/copper-cli/compare/1.0.6...1.0.7) (2022-08-04)


### Bug Fixes

* Search parameters still use *int instead of *int64 ([2497a78](https://gitlab.com/capinside/copper-cli/commit/2497a78b38b18baad0df4924932b2c7be4c60c9a))

## [1.0.6](https://gitlab.com/capinside/copper-cli/compare/1.0.5...1.0.6) (2022-08-04)


### Bug Fixes

* Date values are int instead of int64 ([9f15c19](https://gitlab.com/capinside/copper-cli/commit/9f15c19a2abe8238f9325782d63155f995d3574d))

## [1.0.5](https://gitlab.com/capinside/copper-cli/compare/1.0.4...1.0.5) (2022-08-04)


### Bug Fixes

* Update of single resource attribute not possible ([92074ef](https://gitlab.com/capinside/copper-cli/commit/92074ef49192cc9d4c2c0afb657331a8abff92d5))

## [1.0.4](https://gitlab.com/capinside/copper-cli/compare/1.0.3...1.0.4) (2022-08-03)


### Features

* CustomFieldDefinition.IsCustomFieldDefinitionAvailableOn method ([a4b5842](https://gitlab.com/capinside/copper-cli/commit/a4b58422b7bdb9f6efde3fc9145d595f4b064713))

## [1.0.3](https://gitlab.com/capinside/copper-cli/compare/1.0.2...1.0.3) (2022-07-21)


### Bug Fixes

* LeadConvertResponse typo 'Comapny' instead of 'Company' ([42db122](https://gitlab.com/capinside/copper-cli/commit/42db1222fb3259e465608cbb26d3d022361b9626))

## [1.0.2](https://gitlab.com/capinside/copper-cli/compare/1.0.1...1.0.2) (2022-07-13)


### Bug Fixes

* Constant CostomFieldDefinitionsPath must be named CustomFieldDefinitionsPath ([1d8e3b1](https://gitlab.com/capinside/copper-cli/commit/1d8e3b1fee8e87d54dff372f1caaeb71bec3f3e5))

## [1.0.1](https://gitlab.com/capinside/copper-cli/compare/1.0.0...1.0.1) (2022-06-27)

# 1.0.0 (2022-06-23)


### Bug Fixes

* gcc not found in Gitlab CI job test ([20896fa](https://gitlab.com/capinside/copper-cli/commit/20896fa64d8ca3af0889ed4dc4fae4e70667cbe1))
* musl-dev missing in Gitlab CI job test ([64d391a](https://gitlab.com/capinside/copper-cli/commit/64d391a1056c8b7c796bea3d0ae4091219e879ef))
* Persons DateModified is int instead of int64 ([411001d](https://gitlab.com/capinside/copper-cli/commit/411001d40030322fd0769f783bf20d422b679c73))


### Features

* Add Gitlab CI job to create a release and CHANGELOG.md ([c0ccd45](https://gitlab.com/capinside/copper-cli/commit/c0ccd45a59bb58bddb052ed746f5a4240859e54d))
* Add global unexported client and global methods ([0ab5348](https://gitlab.com/capinside/copper-cli/commit/0ab5348e046cb329511df89313f341e0622f00e7))
