package copper

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func setupTest(pattern string, statusCode int, body []byte) *httptest.Server {
	mux := http.NewServeMux()
	mux.HandleFunc(pattern, func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
		_, err := w.Write(body)
		if err != nil {
			panic(err)
		}
	})
	server := httptest.NewServer(mux)

	// Set 'error' to 'debug' to see json data
	if err := Init(server.URL, "", "", "error"); err != nil {
		panic(err)
	}

	return server
}

func TestInit(t *testing.T) {
	assert.NotNil(t, c)
}

func TestLookupEnv(t *testing.T) {
	tt := []struct {
		SetEnv bool
		Key    string
		Value  string
		Result string
	}{
		{
			SetEnv: true,
			Key:    EnvironmentVariableAPIURL,
			Value:  BaseURLV1,
			Result: BaseURLV1,
		},
		{
			SetEnv: false,
			Key:    "FOOBAR",
			Value:  "test default value",
			Result: "test default value",
		},
	}

	for i, test := range tt {
		if test.SetEnv {
			os.Setenv(test.Key, test.Value)
		}

		result := lookupEnv(test.Key, test.Value)
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\ntest: %d\nwant: %v\ngot: %v", i, test.Result, result)
		}
	}
}

func TestNewClient(t *testing.T) {
	client := NewClient(Config{})
	assert.NotNil(t, client)
}

func TestNewConfig(t *testing.T) {
	config, err := NewConfig("http://localhost:8080", "test account", "test token")
	assert.Nil(t, err)
	require.NotNil(t, config)
	assert.Equal(t, "http://localhost:8080", config.BaseURL.String())
	assert.Equal(t, "test account", config.Account)
	assert.Equal(t, "test token", config.Token)
}

func TestSetHeader(t *testing.T) {
	tt := []struct {
		Account string
		Token   string
		Result  http.Header
	}{
		{
			Account: "",
			Token:   "",
			Result: http.Header{
				"Content-Type":     []string{"application/json; charset=UTF-8"},
				"X-Pw-Accesstoken": []string{""},
				"X-Pw-Application": []string{"developer_api"},
				"X-Pw-Useremail":   []string{""},
			},
		},
		{
			Account: "test-account",
			Token:   "test-token",
			Result: http.Header{
				"Content-Type":     []string{"application/json; charset=UTF-8"},
				"X-Pw-Accesstoken": []string{"test-token"},
				"X-Pw-Application": []string{"developer_api"},
				"X-Pw-Useremail":   []string{"test-account"},
			},
		},
	}

	for _, test := range tt {
		f := getHeaderFunc(test.Account, test.Token)
		result := f()
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}
	}
}

func TestHandleAPIError(t *testing.T) {
	tt := []struct {
		Response *http.Response
		Body     []byte
		Error    error
		Result   []byte
	}{
		{
			Response: &http.Response{
				StatusCode: http.StatusNotFound,
			},
			Body:  []byte(`{"success":false,"status":404,"message":"Resource not found"}`),
			Error: ErrResourceNotFound,
		},
		{
			Response: &http.Response{
				StatusCode: http.StatusUnauthorized,
			},
			Body:  []byte(`{"error":"authentication error"}`),
			Error: ErrAuthentication,
		},
		{
			Response: &http.Response{
				StatusCode: http.StatusUnprocessableEntity,
			},
			Body:  []byte(`{"success":false,"status":422,"message":"Invalid input: Validation errors: Monetary Unit: Invalid value for Monetary Unit; Currency is not a valid ISO currency"}`),
			Error: fmt.Errorf("Invalid input: Validation errors: Monetary Unit: Invalid value for Monetary Unit; Currency is not a valid ISO currency"),
		},
		{
			Response: &http.Response{
				StatusCode: http.StatusOK,
			},
			Body:   []byte(`{"foo":"bar"}`),
			Error:  nil,
			Result: []byte(`{"foo":"bar"}`),
		},
	}

	for _, test := range tt {
		result, err := handleAPIResponse(test.Response, test.Body)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error.Error(), err.Error())
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}
	}
}
