package copper

import (
	"encoding/json"
	"fmt"

	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

const (
	// BaseURLV1 URL of Copper API v1
	BaseURLV1 = "https://api.copper.com/developer_api/v1"
	// MaxRequestPageSize is the maximum entries returned per page by API
	MaxRequestPageSize int64 = 200
)

// DeleteResourceByIDFunc function header
type DeleteResourceByIDFunc func(string, int64) (*copperv1.DeleteResponse, error)

func deleteResourceByIDFunc(client Client) DeleteResourceByIDFunc {
	return func(resource string, id int64) (*copperv1.DeleteResponse, error) {
		path := fmt.Sprintf("%s/%d", resource, id)

		resp, err := client.Delete(path, nil)
		if err != nil {
			return nil, err
		}

		result := &copperv1.DeleteResponse{}
		return result, json.Unmarshal(resp, &result)

	}
}
