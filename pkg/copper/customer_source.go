package copper

import (
	"encoding/json"

	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

const (
	// CustomerSourcesPath on API
	CustomerSourcesPath = "/customer_sources"
)

var (
	// CustomerSources is the global customer sources services
	CustomerSources CustomerSourcesService
)

// CustomerSourcesService interface
type CustomerSourcesService interface {
	CustomerSources() ([]*copperv1.CustomerSource, error)
	FetchByID(int64) (*copperv1.CustomerSource, error)
	FetchByName(string) (*copperv1.CustomerSource, error)
}

// CustomerSourcesFunc function header
type CustomerSourcesFunc func() ([]*copperv1.CustomerSource, error)

type customerSourcesService struct {
	customerSourcesFunc CustomerSourcesFunc
}

// NewCustomerSourcesService returns an instance of customerSourcesServices
// that implements
func NewCustomerSourcesService(client Client) CustomerSourcesService {
	return &customerSourcesService{
		customerSourcesFunc: customerSourcesFunc(client),
	}
}

// CustomerSources returns a slice of available CustomSources and nil or nil and an error
func (css *customerSourcesService) CustomerSources() ([]*copperv1.CustomerSource, error) {
	return css.customerSourcesFunc()
}

func customerSourcesFunc(client Client) CustomerSourcesFunc {
	return func() ([]*copperv1.CustomerSource, error) {
		data, err := client.Get(CustomerSourcesPath, nil)
		if err != nil {
			return nil, err
		}

		result := []*copperv1.CustomerSource{}
		return result, json.Unmarshal(data, &result)
	}
}

func fetchCustomerSourceBy(css CustomerSourcesService, compareFunc func(*copperv1.CustomerSource) bool) (*copperv1.CustomerSource, error) {
	customerSources, err := css.CustomerSources()
	if err != nil {
		return nil, err
	}

	for _, customerSource := range customerSources {
		if compareFunc(customerSource) {
			return customerSource, nil
		}
	}

	return nil, nil
}

// FetchByName returns the pipeline matching name and nil or nil and an error
func (css *customerSourcesService) FetchByName(name string) (*copperv1.CustomerSource, error) {
	return fetchCustomerSourceBy(css, func(p *copperv1.CustomerSource) bool {
		return p.Name == name
	})
}

// FetchByID returns the customer source matching id and nil or nil and an error
func (css *customerSourcesService) FetchByID(id int64) (*copperv1.CustomerSource, error) {
	return fetchCustomerSourceBy(css, func(p *copperv1.CustomerSource) bool {
		return p.Id != nil && *p.Id == id
	})
}

// GetCustomerSources returns a slice of CustomerSources and nil
// or nil and an error
func GetCustomerSources() ([]*copperv1.CustomerSource, error) {
	return CustomerSources.CustomerSources()
}

// FetchCustomerSourceByID returns a pointer of CustomerSource matching id and nil
// or nil and an error
func FetchCustomerSourceByID(id int64) (*copperv1.CustomerSource, error) {
	return CustomerSources.FetchByID(id)
}

// FetchCustomerSourceByName returns a pointer of CustomerSource matching name and nil
// or nil and an error
func FetchCustomerSourceByName(name string) (*copperv1.CustomerSource, error) {
	return CustomerSources.FetchByName(name)
}
