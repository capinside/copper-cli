package copper

import (
	"encoding/json"

	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

const (
	// PipelinesPath on API
	PipelinesPath = "/pipelines"
)

var (
	// Pipelines is the global pipeline service
	Pipelines PipelinesService
)

// PipelinesService interface
type PipelinesService interface {
	FetchByID(int64) (*copperv1.Pipeline, error)
	FetchByName(string) (*copperv1.Pipeline, error)
	Search(copperv1.PipelineSearchParams) ([]*copperv1.Pipeline, error)
}

// PipelineFetchByIDFunc function header
type PipelineFetchByIDFunc func(int64) (*copperv1.Pipeline, error)

// PipelineFetchByNameFunc function header
type PipelineFetchByNameFunc func(string) (*copperv1.Pipeline, error)

// PipelinesSearchFunc function header
type PipelinesSearchFunc func(copperv1.PipelineSearchParams) ([]*copperv1.Pipeline, error)

type pipelinesService struct {
	fetchByIDFunc   PipelineFetchByIDFunc
	fetchByNameFunc PipelineFetchByNameFunc
	searchFunc      PipelinesSearchFunc
}

// NewPipelinesService returns an instance of pipelinesService that implements
// the PipelinesService interface
func NewPipelinesService(client Client) PipelinesService {
	return NewPipelinesServiceWithFunc(
		pipelineFetchByIDFunc(client),
		pipelineFetchByNameFunc(client),
		pipelinesSearchFunc(client),
	)
}

// NewPipelinesServiceWithFunc returns an instance of pipelinesService that implements
// the PipelinesService interface and has fetchByIDFunc and searchFunc set.
func NewPipelinesServiceWithFunc(fetchByIDFunc PipelineFetchByIDFunc, fetchByNameFunc PipelineFetchByNameFunc, searchFunc PipelinesSearchFunc) PipelinesService {
	return &pipelinesService{
		fetchByIDFunc:   fetchByIDFunc,
		fetchByNameFunc: fetchByNameFunc,
		searchFunc:      searchFunc,
	}
}

// FetchByID returns the pipeline matching id and nil or nil and an error
func (ps *pipelinesService) FetchByID(id int64) (*copperv1.Pipeline, error) {
	return ps.fetchByIDFunc(id)
}

func pipelineFetchByIDFunc(client Client) PipelineFetchByIDFunc {
	return func(id int64) (*copperv1.Pipeline, error) {
		return fetchPipelineByFunc(client)(func(p *copperv1.Pipeline) bool {
			return p.Id == id
		})
	}
}

// FetchByName returns the pipeline matching name and nil or nil and an error
func (ps *pipelinesService) FetchByName(name string) (*copperv1.Pipeline, error) {
	return ps.fetchByNameFunc(name)
}

func pipelineFetchByNameFunc(client Client) PipelineFetchByNameFunc {
	return func(name string) (*copperv1.Pipeline, error) {
		return fetchPipelineByFunc(client)(func(p *copperv1.Pipeline) bool {
			return p.Name == name
		})
	}
}

func fetchPipelineByFunc(client Client) func(func(*copperv1.Pipeline) bool) (*copperv1.Pipeline, error) {
	return func(compareFunc func(*copperv1.Pipeline) bool) (*copperv1.Pipeline, error) {
		searchFunc := pipelinesSearchFunc(client)
		pipelines, err := searchFunc(copperv1.PipelineSearchParams{})
		if err != nil {
			return nil, err
		}

		for _, pipeline := range pipelines {
			if compareFunc(pipeline) {
				return pipeline, nil
			}
		}

		return nil, nil
	}
}

// Search returns a slice fo pipelines matching params and nil
// or nil and an error
func (ps *pipelinesService) Search(params copperv1.PipelineSearchParams) ([]*copperv1.Pipeline, error) {
	return ps.searchFunc(params)
}

func pipelinesSearchFunc(client Client) PipelinesSearchFunc {
	return func(params copperv1.PipelineSearchParams) ([]*copperv1.Pipeline, error) {
		data, err := client.Get(PipelinesPath, &params)
		if err != nil {
			return nil, err
		}

		result := []*copperv1.Pipeline{}
		return result, json.Unmarshal(data, &result)
	}
}

// FetchPipelineByID returns a pointer of the Pipeline matching id and nil
// or nil and an error
func FetchPipelineByID(id int64) (*copperv1.Pipeline, error) {
	return Pipelines.FetchByID(id)
}

// FetchPipelineByName returns a pointer of the Pipeline matching name and nil
// or nil and an error
func FetchPipelineByName(name string) (*copperv1.Pipeline, error) {
	return Pipelines.FetchByName(name)
}

// SearchPipelines returns a slice of Pipeline matching params and nil
// or nil and an error
func SearchPipelines(params copperv1.PipelineSearchParams) ([]*copperv1.Pipeline, error) {
	return Pipelines.Search(params)
}
