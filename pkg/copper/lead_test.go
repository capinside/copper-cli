package copper

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

func TestNewLeadsService(t *testing.T) {
	ls := NewLeadsService(nil)
	assert.NotNil(t, ls)
}

func TestSearchLeads(t *testing.T) {
	tt := []struct {
		Params     copperv1.LeadSearchParams
		Body       []byte
		StatusCode int
		Result     []*copperv1.Lead
		Error      error
	}{
		{
			Body:       []byte(`[]`),
			StatusCode: http.StatusOK,
			Result:     []*copperv1.Lead{},
			Error:      nil,
		},
		{
			Params: copperv1.LeadSearchParams{
				Emails: "pinky@acmelabs.com",
			},
			Body:       []byte(`[{"id":12345,"name":"Pinky","prefix":null,"first_name":"Pinky","last_name":"Acme","middle_name":null,"suffix":null,"address":{"street":"Mainstreet","city":"ACME Town","state":"ACME State","postal_code":"12345","country":"ACME Land"},"assignee_id":null,"company_name":"ACME Labs","email":{"email":"pinky@acmelabs.com","category":"work"},"interaction_count":12}]`),
			StatusCode: http.StatusOK,
			Result: []*copperv1.Lead{
				{
					Id:         utils.Int64Addr(12345),
					Name:       "Pinky",
					Prefix:     "",
					FirstName:  "Pinky",
					LastName:   "Acme",
					MiddleName: "",
					Suffix:     "",
					Address: &copperv1.Address{
						Street:     "Mainstreet",
						City:       "ACME Town",
						State:      "ACME State",
						PostalCode: "12345",
						Country:    "ACME Land",
					},
					CompanyName: "ACME Labs",
					Email: &copperv1.Email{
						Email:    "pinky@acmelabs.com",
						Category: "work",
					},
					InteractionCount: utils.Int64Addr(12),
				},
			},
		},
	}

	for _, test := range tt {
		server := setupTest(LeadsSearchPath, test.StatusCode, test.Body)

		result, err := SearchLeads(test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestCreateLead(t *testing.T) {
	tt := []struct {
		Body       []byte
		Data       copperv1.Lead
		StatusCode int
		Result     *copperv1.Lead
		Error      error
	}{
		{
			Body:       []byte(`{"message":"test message","status":422}`),
			Data:       copperv1.Lead{},
			StatusCode: http.StatusUnprocessableEntity,
			Error:      fmt.Errorf("test message"),
			Result:     nil,
		},
		{
			Body: []byte(`{"id":1,"first_name":"John","last_name":"Doe","email":{"email":"johndoe@example.net","category":"work"}}`),
			Data: copperv1.Lead{
				FirstName: "John",
				LastName:  "Doe",
				Email: &copperv1.Email{
					Email:    "johndoe@example.net",
					Category: "work",
				},
			},
			StatusCode: http.StatusOK,
			Result: &copperv1.Lead{
				Id:        utils.Int64Addr(1),
				FirstName: "John",
				LastName:  "Doe",
				Email: &copperv1.Email{
					Email:    "johndoe@example.net",
					Category: "work",
				},
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setupTest(LeadsPath, test.StatusCode, test.Body)

		result, err := CreateLead(test.Data)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err.Error())
			}
			if err.Error() != test.Error.Error() {
				t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
			}
		}

		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestUpdateLead(t *testing.T) {
	tt := []struct {
		Body       []byte
		StatusCode int
		ID         int64
		Data       copperv1.Lead
		Result     *copperv1.Lead
		Error      error
	}{
		{
			ID:         1,
			Body:       []byte(`{"id":1,"first_name":"John","last_name":"Doe"}`),
			StatusCode: http.StatusOK,
			Data: copperv1.Lead{
				FirstName: "John",
				LastName:  "Doe",
			},
			Result: &copperv1.Lead{
				Id:        utils.Int64Addr(1),
				FirstName: "John",
				LastName:  "Doe",
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d", LeadsPath, test.ID), test.StatusCode, test.Body)

		result, err := UpdateLead(test.ID, test.Data)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}

		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", &test.Result, result)
		}

		server.Close()
	}
}

func TestFecthLeadByID(t *testing.T) {
	tt := []struct {
		ID         int64
		Body       []byte
		StatusCode int
		Result     *copperv1.Lead
		Error      error
	}{
		{
			ID:         1234567890,
			Body:       []byte(`{"success":false,"status":404,"message":"Resource not found"}`),
			StatusCode: http.StatusNotFound,
			Result:     nil,
			Error:      ErrResourceNotFound,
		},
		{
			ID:         1,
			Body:       []byte(`{"id":1,"first_name":"John","last_name":"Doe"}`),
			StatusCode: http.StatusOK,
			Result: &copperv1.Lead{
				Id:        utils.Int64Addr(1),
				FirstName: "John",
				LastName:  "Doe",
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d", LeadsPath, test.ID), test.StatusCode, test.Body)

		result, err := FetchLeadByID(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestDeleteLead(t *testing.T) {
	tt := []struct {
		ID         int64
		Body       []byte
		Result     *copperv1.DeleteResponse
		StatusCode int
		Error      error
	}{
		{
			ID:         1234567890,
			Body:       []byte(`{"success":false,"status":404,"message":"Resource not found"}`),
			StatusCode: http.StatusNotFound,
			Error:      ErrResourceNotFound,
		},
		{
			ID:         71258463,
			Body:       []byte(`{"id":71258463,"is_deleted":true}`),
			StatusCode: http.StatusOK,
			Result: &copperv1.DeleteResponse{
				Id:        71258463,
				IsDeleted: true,
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d", LeadsPath, test.ID), test.StatusCode, test.Body)

		result, err := DeleteLead(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestLeadActivities(t *testing.T) {
	tt := []struct {
		ID         int64
		Params     copperv1.ActivitySearchParams
		Body       []byte
		StatusCode int
		Error      error
		Result     []*copperv1.Activity
	}{
		{
			ID:         1,
			Params:     copperv1.ActivitySearchParams{},
			Body:       []byte(`[{"id":68,"parent":{"id":1,"type":"lead"},"type":{"id":1,"category":"user"},"user_id":1,"details":"Phone call with Dave","activity_date":1522455323,"old_value":null,"new_value":null,"date_created":1522455329,"date_modified":1522455323}]`),
			StatusCode: http.StatusOK,
			Error:      nil,
			Result: []*copperv1.Activity{
				{
					Id: utils.Int64Addr(68),
					Parent: &copperv1.ActivityParent{
						Id:   1,
						Type: "lead",
					},
					Type: &copperv1.ActivityType{
						Id:       1,
						Category: "user",
					},
					UserId:       utils.Int64Addr(1),
					Details:      "Phone call with Dave",
					ActivityDate: utils.Int64Addr(1522455323),
					DateCreated:  utils.Int64Addr(1522455329),
					DateModified: utils.Int64Addr(1522455323),
				},
			},
		},
	}

	for _, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d/activities", LeadsPath, test.ID), test.StatusCode, test.Body)

		result, err := LeadActivities(test.ID, test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestConvertLead(t *testing.T) {
	tt := []struct {
		Body       []byte
		ID         int64
		Data       copperv1.LeadConvertData
		Result     *copperv1.LeadConvertResponse
		StatusCode int
		Error      error
	}{
		{
			Body:       []byte(`{"person":{"id":27140359,"name":"My Contact","prefix":null,"first_name":"My","middle_name":null,"last_name":"Contact","suffix":null,"address":null,"assignee_id":null,"company_id":13349319,"company_name":"Noemail","contact_type_id":451492,"details":null,"emails":[{"email":"mylead@noemail.com","category":"work"}],"phone_numbers":[],"socials":[],"tags":[],"title":null,"websites":[],"custom_fields":[{"custom_field_definition_id":100764},{"custom_field_definition_id":103481}],"date_created":1490045010,"date_modified":1496694264,"interaction_count":0},"company":{"id":13349319,"name":"Noemail","address":null,"assignee_id":137658,"contact_type_id":451490,"details":null,"email_domain":"noemail.com","phone_numbers":[],"socials":[],"tags":[],"websites":[],"custom_fields":[{"custom_field_definition_id":100764},{"custom_field_definition_id":103481}],"interaction_count":0,"date_created":1496694264,"date_modified":1496694264},"opportunity":{"id":4417020,"name":"Demo Project","assignee_id":null,"close_date":null,"company_id":13349319,"company_name":"Noemail","customer_source_id":null,"details":"","loss_reason_id":null,"pipeline_id":213214,"pipeline_stage_id":987790,"primary_contact_id":27140359,"priority":null,"status":"Open","tags":[],"interaction_count":0,"monetary_value":1000,"win_probability":0,"date_created":1496694264,"date_modified":1496694264,"custom_fields":[{"custom_field_definition_id":100764},{"custom_field_definition_id":103481}]}}`),
			ID:         1234567890,
			StatusCode: http.StatusOK,
			Data:       copperv1.LeadConvertData{},
			Result: &copperv1.LeadConvertResponse{
				Person: &copperv1.Person{
					CompanyId:     utils.Int64Addr(13349319),
					CompanyName:   "Noemail",
					ContactTypeId: utils.Int64Addr(451492),
					CustomFields: []*copperv1.CustomField{
						{
							CustomFieldDefinitionId: 100764,
							// Value:                   "Text fields are 255 chars or less!",
						},
						{
							CustomFieldDefinitionId: 103481,
							// Value:                   "Text area fields can have long text content",
						},
					},
					DateCreated:  utils.Int64Addr(1490045010),
					DateModified: utils.Int64Addr(1496694264),
					Emails: []*copperv1.Email{
						{
							Email:    "mylead@noemail.com",
							Category: "work",
						},
					},
					FirstName:        "My",
					Id:               27140359,
					InteractionCount: utils.Int64Addr(0),
					LastName:         "Contact",
					Name:             "My Contact",
					PhoneNumbers:     []*copperv1.PhoneNumber{},
					Socials:          []*copperv1.Social{},
					Tags:             []string{},
					Websites:         []*copperv1.Website{},
				},
				Company: &copperv1.Company{
					AssigneeId:    utils.Int64Addr(137658),
					ContactTypeId: utils.Int64Addr(451490),
					CustomFields: []*copperv1.CustomField{
						{
							CustomFieldDefinitionId: 100764,
							// Value:                   "Text fields are 255 chars or less!",
						},
						{
							CustomFieldDefinitionId: 103481,
							// Value:                   "Text area fields can have long text content",
						},
					},
					DateCreated:      utils.Int64Addr(1496694264),
					DateModified:     utils.Int64Addr(1496694264),
					EmailDomain:      "noemail.com",
					Id:               utils.Int64Addr(13349319),
					InteractionCount: utils.Int64Addr(0),
					Name:             "Noemail",
					PhoneNumbers:     []*copperv1.PhoneNumber{},
					Socials:          []*copperv1.Social{},
					Tags:             []string{},
					Websites:         []*copperv1.Website{},
				},
				Opportunity: &copperv1.Opportunity{
					CompanyId:   utils.Int64Addr(13349319),
					CompanyName: "Noemail",
					CustomFields: []*copperv1.CustomField{
						{
							CustomFieldDefinitionId: 100764,
							// Value:                   "Text fields are 255 chars or less!",
						},
						{
							CustomFieldDefinitionId: 103481,
							// Value:                   "Text area fields can have long text content",
						},
					},
					DateCreated:      utils.Int64Addr(1496694264),
					DateModified:     utils.Int64Addr(1496694264),
					Id:               utils.Int64Addr(4417020),
					InteractionCount: utils.Int64Addr(0),
					MonetaryValue:    utils.Float64Addr(1000),
					Name:             "Demo Project",
					PipelineId:       213214,
					PipelineStageId:  987790,
					PrimaryContactId: 27140359,
					Status:           "Open",
					Tags:             []string{},
					WinProbability:   0,
				},
			},
		},
	}

	for i, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d/convert", LeadsPath, test.ID), test.StatusCode, test.Body)

		result, err := ConvertLead(test.ID, test.Data)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\ntest: %d\nunexpected error: %v", i, err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Result, result)
		}

		server.Close()
	}
}

func TestUpsertLead(t *testing.T) {
	tt := []struct {
		Data       copperv1.LeadUpsertData
		StatusCode int
		Body       []byte
		Result     *copperv1.Lead
		Error      error
	}{
		{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf("%s", http.StatusText(http.StatusInternalServerError)),
		},
		{
			Data: copperv1.LeadUpsertData{
				Properties: &copperv1.LeadUpsertProperties{
					Name: "John Doe",
					Email: &copperv1.Email{
						Email:    "johndoe@example.org",
						Category: "work",
					},
				},
				Match: &copperv1.LeadUpsertMatch{
					FieldName:  "email",
					FieldValue: "johndoe@example.org",
				},
			},
			StatusCode: http.StatusOK,
			Body:       []byte(`{"id":8982702,"name":"John Doe","email":{"email":"johndoe@example.org","category":"work"}}`),
			Result: &copperv1.Lead{
				Id:   utils.Int64Addr(8982702),
				Name: "John Doe",
				Email: &copperv1.Email{
					Email:    "johndoe@example.org",
					Category: "work",
				},
			},
		},
	}

	for i, test := range tt {
		server := setupTest(fmt.Sprintf("%s/upsert", LeadsPath), test.StatusCode, test.Body)

		result, err := UpsertLead(test.Data)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Result, result)
		}

		server.Close()

	}
}
