package copper

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

func TestFetchPersonByEmail(t *testing.T) {
	tt := []struct {
		Email      string
		Body       []byte
		StatusCode int
		Result     *copperv1.Person
		Error      error
	}{
		{
			Body:       []byte("{}"),
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			Body:       []byte(`{"success":false,"status":404,"message":"Resource not found"}`),
			StatusCode: http.StatusNotFound,
			Error:      ErrResourceNotFound,
			Result:     nil,
		},
		{
			Email:      "john.doe@example.net",
			Body:       []byte(`{"id":1,"name":"John Doe","first_name":"John","last_name":"Doe","emails":[{"email":"john.doe@example.net","category":"work"}]}`),
			StatusCode: http.StatusOK,
			Result: &copperv1.Person{
				Id:        1,
				Name:      "John Doe",
				FirstName: "John",
				LastName:  "Doe",
				Emails: []*copperv1.Email{
					{
						Email:    "john.doe@example.net",
						Category: "work",
					},
				},
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setupTest(PersonsFetchByEmailPath, test.StatusCode, test.Body)

		result, err := FetchPersonByEmail(test.Email)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestFetchPersonByID(t *testing.T) {
	tt := []struct {
		ID         int64
		Body       []byte
		StatusCode int
		Result     *copperv1.Person
		Error      error
	}{
		{
			ID:         1,
			Body:       []byte(`{"id":1,"name":"John Doe","first_name":"John","last_name":"Doe","emails":[{"email":"john.doe@example.net","category":"work"}]}`),
			StatusCode: http.StatusOK,
			Result: &copperv1.Person{
				Id:        1,
				Name:      "John Doe",
				FirstName: "John",
				LastName:  "Doe",
				Emails: []*copperv1.Email{
					{
						Email:    "john.doe@example.net",
						Category: "work",
					},
				},
			},
			Error: nil,
		},
		{
			ID:         1234567890,
			Body:       []byte(`{"success":false,"status":422,"message":"Invalid input"}`),
			StatusCode: http.StatusUnprocessableEntity,
			Result:     nil,
			Error:      ErrInvalidInput,
		},
	}

	for _, test := range tt {
		server := setupTest(PersonsPathWithID(test.ID), test.StatusCode, test.Body)

		result, err := FetchPersonByID(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\nwant: %v\ngot:  nil", err)
			}
			if err.Error() != test.Error.Error() {
				t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
			}
		}

		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestSearchAllPersons(t *testing.T) {
	tt := []struct {
		Params     copperv1.PersonSearchParams
		Body       []byte
		StatusCode int
		Result     []*copperv1.Person
		Error      error
	}{
		{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			Body:       []byte(`[]`),
			StatusCode: http.StatusOK,
			Result:     []*copperv1.Person{},
		},
		{
			Params: copperv1.PersonSearchParams{
				Emails: []string{"johndoe@example.net"},
			},
			Body:       []byte(`[{"id":1,"first_name":"John","last_name":"Doe","emails":[{"email":"johndoe@example.net","category":"work"}]}]`),
			StatusCode: http.StatusOK,
			Result: []*copperv1.Person{
				{
					Id:        1,
					FirstName: "John",
					LastName:  "Doe",
					Emails: []*copperv1.Email{
						{
							Email:    "johndoe@example.net",
							Category: "work",
						},
					},
				},
			},
		},
	}

	for _, test := range tt {
		server := setupTest(PersonsSearchPath, test.StatusCode, test.Body)

		result, err := SearchAllPersons(test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestCreatePerson(t *testing.T) {
	tt := []struct {
		Data       copperv1.Person
		Body       []byte
		StatusCode int
		Result     *copperv1.Person
		Error      error
	}{
		{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		// 		{
		// 			Data: Person{
		// 				FirstName: "John",
		// 				LastName:  "Doe",
		// 				Emails: []*copperv1.Email{
		// 					{
		// 						Email:    "johndoe@example.net",
		// 						Category: "work",
		// 					},
		// 				},
		// 			},
		// 			Body:       []byte(`{"id":1,"first_name":"John","last_name":"Doe","emails":[{"email":"johndoe@example.net","category":"work"}]}`),
		// 			StatusCode: http.StatusOK,
		// 			Result: &copperv1.Person{
		// 				ID:        1,
		// 				FirstName: "John",
		// 				LastName:  "Doe",
		// 				Emails: []*copperv1.Email{
		// 					{
		// 						Email:    "johndoe@example.net",
		// 						Category: "work",
		// 					},
		// 				},
		// 			},
		// 			Error: nil,
		// 		},
	}

	for _, test := range tt {
		server := setupTest(PersonsPath, test.StatusCode, test.Body)

		result, err := CreatePerson(test.Data)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

// func TestDeletePerson(t *testing.T) {
// 	tt := []struct {
// 		ID         int64
// 		Body       []byte
// 		Result     *copperv1.DeleteResponse
// 		StatusCode int
// 		Error      error
// 	}{
// 		{
// 			ID:         1234567890,
// 			Body:       []byte(`{"success":false,"status":404,"message":"Resource not found"}`),
// 			StatusCode: http.StatusNotFound,
// 			Error:      fmt.Errorf("%s: Resource not found", http.StatusText(http.StatusNotFound)),
// 		},
// 		{
// 			ID:         71258463,
// 			Body:       []byte(`{"id":71258463,"is_deleted":true}`),
// 			StatusCode: http.StatusOK,
// 			Result: &copperv1.DeleteResponse{
// 				ID:        71258463,
// 				IsDeleted: true,
// 			},
// 			Error: nil,
// 		},
// 	}

// 	for _, test := range tt {
// 		server := setupTest(PersonsPathWithID(test.ID), test.StatusCode, test.Body)

// 		result, err := DeletePerson(test.ID)
// 		if err != nil {
// 			if test.Error == nil {
// 				t.Fatalf("unexpected error: %v", err)
// 			} else {
// 				if err.Error() != test.Error.Error() {
// 					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
// 				}
// 			}
// 		}
// 		if !assert.Equal(t, test.Result, result) {
// 			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
// 		}

// 		server.Close()
// 	}
// }

// func TestUpdatePerson(t *testing.T) {
// 	tt := []struct {
// 		ID         int64
// 		Data       copperv1.Person
// 		Body       []byte
// 		Result     *copperv1.Person
// 		StatusCode int
// 		Error      error
// 	}{
// 		{
// 			ID: 1,
// 			Data: copperv1.Person{
// 				FirstName: "John",
// 			},
// 			Body:       []byte(`{"id":1,"first_name":"John"}`),
// 			StatusCode: http.StatusOK,
// 			Result: &copperv1.Person{
// 				ID:        1,
// 				FirstName: "John",
// 			},
// 			Error: nil,
// 		},
// 	}

// 	for _, test := range tt {
// 		server := setupTest(PersonsPathWithID(test.ID), test.StatusCode, test.Body)

// 		result, err := UpdatePerson(test.ID, test.Data)
// 		if err != nil {
// 			if test.Error == nil {
// 				t.Fatalf("unexpected error: %v", err)
// 			} else {
// 				if err.Error() != test.Error.Error() {
// 					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
// 				}
// 			}
// 		}
// 		if !assert.Equal(t, test.Result, result) {
// 			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
// 		}

// 		server.Close()
// 	}
// }

// func TestPersonActivities(t *testing.T) {
// 	tt := []struct {
// 		ID         int64
// 		Params     copperv1.ActivitySearchParams
// 		Body       []byte
// 		StatusCode int
// 		Error      error
// 		Result     []Activity
// 	}{
// 		{
// 			ID:         1,
// 			Params:     copperv1.ActivitySearchParams{},
// 			Body:       []byte(`[{"id":68,"parent":{"id":1,"type":"person"},"type":{"id":1,"category":"user"},"user_id":1,"details":"Phone call with Dave","activity_date":1522455323,"old_value":null,"new_value":null,"date_created":1522455329,"date_modified":1522455323}]`),
// 			StatusCode: http.StatusOK,
// 			Error:      nil,
// 			Result: []*copperv1.Activity{
// 				{
// 					ID: utils.Int64Addr(68),
// 					Parent: copperv1.ActivityParent{
// 						ID:   1,
// 						Type: "person",
// 					},
// 					Type: copperv1.ActivityType{
// 						ID:       1,
// 						Category: "user",
// 					},
// 					UserID:       utils.Int64Addr(1),
// 					Details:      "Phone call with Dave",
// 					ActivityDate: utils.Int64Addr(1522455323),
// 					DateCreated:  utils.Int64Addr(1522455329),
// 					DateModified: utils.Int64Addr(1522455323),
// 				},
// 			},
// 		},
// 	}

// 	for _, test := range tt {
// 		server := setupTest(fmt.Sprintf("%s/activities", PersonsPathWithID(test.ID)), test.StatusCode, test.Body)

// 		result, err := PersonActivities(test.ID, test.Params)
// 		if err != nil {
// 			if test.Error == nil {
// 				t.Fatalf("unexpected error: %v", err)
// 			} else {
// 				if err.Error() != test.Error.Error() {
// 					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
// 				}
// 			}
// 		}
// 		if !assert.Equal(t, test.Result, result) {
// 			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
// 		}

// 		server.Close()
// 	}
// }

func TestPersonsPathWithID(t *testing.T) {
	s := PersonsPathWithID(1)
	assert.NotEqual(t, "people/1", s)
}
