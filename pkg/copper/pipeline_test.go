package copper

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

func TestNewPipelinesService(t *testing.T) {
	ps := NewPipelinesService(nil)
	assert.NotNil(t, ps)
}

func TestSearchPipelines(t *testing.T) {
	tt := []struct {
		Params     copperv1.PipelineSearchParams
		Body       []byte
		StatusCode int
		Result     []*copperv1.Pipeline
		Error      error
	}{
		{
			Params:     copperv1.PipelineSearchParams{},
			Body:       []byte(`[{"id":1,"name":"Lead Creation Sales","stages":[{"id":1,"name":"Qualified","win_probability":5}]}]`),
			StatusCode: http.StatusOK,
			Result: []*copperv1.Pipeline{
				{
					Id:   1,
					Name: "Lead Creation Sales",
					Stages: []*copperv1.PipelineStage{
						{
							Id:             1,
							Name:           "Qualified",
							WinProbability: 5,
						},
					},
				},
			},
		},
	}

	for _, test := range tt {
		server := setupTest(PipelinesPath, test.StatusCode, test.Body)

		result, err := SearchPipelines(test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestFetchPipelineByName(t *testing.T) {
	tt := []struct {
		Name       string
		Body       []byte
		StatusCode int
		Result     *copperv1.Pipeline
		Error      error
	}{
		{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			Body:       []byte(`{"success":false,"status":404,"message":"Resource not found"}`),
			StatusCode: http.StatusNotFound,
			Error:      ErrResourceNotFound,
		},
		{
			Name:       "test",
			Body:       []byte(`[{"id":1,"name":"PipeA"},{"id":2,"name":"PipeB"},{"id":3,"name":"PipeC"}]`),
			StatusCode: http.StatusOK,
			Error:      ErrInvalidInput,
		},
		{
			Name:       "PipeB",
			Body:       []byte(`[{"id":1,"name":"PipeA"},{"id":2,"name":"PipeB"},{"id":3,"name":"PipeC"}]`),
			StatusCode: http.StatusOK,
			Result: &copperv1.Pipeline{
				Id:   2,
				Name: "PipeB",
			},
		},
	}

	for i, test := range tt {
		server := setupTest(PipelinesPath, test.StatusCode, test.Body)

		result, err := FetchPipelineByName(test.Name)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\ntest: %d\nunexpected error: %v", i, err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Result, result)
		}

		server.Close()
	}
}
func TestFetchPipelineByID(t *testing.T) {
	tt := []struct {
		ID         int64
		Body       []byte
		StatusCode int
		Result     *copperv1.Pipeline
		Error      error
	}{
		{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			ID:         2,
			Body:       []byte(`[{"id":1,"name":"PipeA"},{"id":2,"name":"PipeB"},{"id":3,"name":"PipeC"}]`),
			StatusCode: http.StatusOK,
			Result: &copperv1.Pipeline{
				Id:   2,
				Name: "PipeB",
			},
		},
	}

	for _, test := range tt {
		server := setupTest(PipelinesPath, test.StatusCode, test.Body)

		result, err := FetchPipelineByID(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}
