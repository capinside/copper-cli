package copper

// docs: https://www.yellowduck.be/posts/handling-unix-timestamps-in-json/
// additional: http://choly.ca/post/go-json-marshalling/

import (
	"strconv"
	"time"
)

// UnixTime defines a timestamp encoded as epoch seconds in JSON
type UnixTime time.Time

// MarshalJSON is used to convert the timestamp in JSON
func (ut UnixTime) MarshalJSON() ([]byte, error) {
	return []byte(strconv.FormatInt(time.Time(ut).Unix(), 10)), nil
}

// UnmarshalJSON is used ti convert the timestamp from JSON
func (ut *UnixTime) UnmarshalJSON(s []byte) error {
	r := string(s)
	q, err := strconv.ParseInt(r, 10, 64)
	if err != nil {
		return err
	}
	*(*time.Time)(ut) = time.Unix(q, 0)
	return nil
}

// Unix returns ut as a Unix time, the number of seconds elasped since
// January 1, 1970 UTC. The result does not depend on the location
// accociated with ut.
func (ut UnixTime) Unix() int64 {
	return time.Time(ut).Unix()
}

// Time returns the JSON time as a time.Time instance in UTC
func (ut UnixTime) Time() time.Time {
	return time.Time(ut).UTC()
}

// String returns t as a formatted string
func (ut UnixTime) String() string {
	return ut.Time().String()
}
